<link rel="apple-touch-icon" href="{{asset('assets/images/saya.webp')}}">
<link rel="shortcut icon" href="{{asset('assets/images/saya.webp')}}">

<!-- Stylesheets -->
<link rel="stylesheet" href="{{asset('assets/global/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-extend.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-sweetalert/sweetalert.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/animsition/animsition.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/asscrollable/asScrollable.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/switchery/switchery.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/intro-js/introjs.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/slidepanel/slidePanel.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/flag-icon-css/flag-icon.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/jvectormap/jquery-jvectormap.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/dashboard/v1.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/switchery/switchery.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/tables/datatable.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/uikit/icons.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/layouts/panel-transition.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/asscrollable/asScrollable.css')}}">

<link rel="stylesheet" href="{{asset('assets/global/vendor/select2/select2.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-select/bootstrap-select.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/multi-select/multi-select.css')}}">

<link rel="stylesheet" href="{{asset('assets/global/vendor/nestable/nestable.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/tasklist/tasklist.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/sortable/sortable.css')}}">


<!-- Fonts -->
<link rel="stylesheet" href="{{asset('assets/global/fonts/font-awesome/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/7-stroke/7-stroke.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/ionicons/ionicons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/material-design/material-design.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/mfglabs/mfglabs.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/open-iconic/open-iconic.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/themify/themify.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/weather-icons/weather-icons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/glyphicons/glyphicons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/octicons/octicons.css')}}">

<link rel="stylesheet" href="{{asset('assets/global/fonts/web-icons/web-icons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/brand-icons/brand-icons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/brand-icons/brand-icons.min.css')}}">
{{-- <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'> --}}

<script src="{{url('assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
<script>
    Breakpoints();
</script>