<!-- Core  -->
<script src="{{url('assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{url('assets/global/vendor/jquery/jquery.js')}}"></script>
<script src="{{url('assets/global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="{{url('assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{url('assets/global/vendor/animsition/animsition.js')}}"></script>
<script src="{{url('assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{url('assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{url('assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="{{url('assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

<!-- Plugins -->
<script src="{{url('assets/global/vendor/switchery/switchery.js')}}"></script>
<script src="{{url('assets/global/vendor/intro-js/intro.js')}}"></script>
<script src="{{url('assets/global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{url('assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{url('assets/global/vendor/skycons/skycons.js')}}"></script>
<script src="{{url('assets/global/vendor/aspieprogress/jquery-asPieProgress.min.js')}}"></script>
<script src="{{url('assets/global/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{url('assets/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js')}}"></script>
<script src="{{url('assets/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>

<script src="{{url('assets/global/vendor/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-scroller/dataTables.scroller.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-responsive/dataTables.responsive.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/dataTables.buttons.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.html5.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.flash.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.print.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.colVis.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js')}}"></script>
<script src="{{url('assets/global/vendor/asrange/jquery-asRange.min.js')}}"></script>
<script src="{{url('assets/global/vendor/bootbox/bootbox.js')}}"></script>
<script src="{{url('assets/global/vendor/switchery/switchery.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/switchery.js')}}"></script>
<script src="{{url('assets/global/vendor/asprogress/jquery-asProgress.js')}}"></script>
<script src="{{url('assets/global/vendor/draggabilly/draggabilly.pkgd.js')}}"></script>
<script src="{{url('assets/global/vendor/raty/jquery.raty.js')}}"></script>

<!-- Scripts -->
<script src="{{url('assets/global/js/Component.js')}}"></script>
<script src="{{url('assets/global/js/Plugin.js')}}"></script>
<script src="{{url('assets/global/js/Base.js')}}"></script>
<script src="{{url('assets/global/js/Config.js')}}"></script>

<script src="{{url('assets/js/Section/Menubar.js')}}"></script>
<script src="{{url('assets/js/Section/GridMenu.js')}}"></script>
<script src="{{url('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{url('assets/js/Section/PageAside.js')}}"></script>
<script src="{{url('assets/js/Plugin/menu.js')}}"></script>

<script src="{{url('assets/global/js/config/colors.js')}}"></script>
<script src="{{url('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '../assets');
</script>

<!-- Page -->
<script src="{{url('assets/js/Site.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/switchery.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/matchheight.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/jvectormap.js')}}"></script>
<script src="{{url('assets/js/Site.js')}}">
</script>
<script src="{{url('assets/global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/slidepanel.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/datatables.js')}}"></script>
<script src="{{url('assets/examples/js/dashboard/v1.js')}}"></script>

<script src="{{url('assets/examples/js/tables/datatable.js')}}">
</script>

<script src="{{url('assets/global/js/Plugin/animate-list.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/panel.js')}}"></script>

<script src="{{url('assets/examples/js/layouts/panel-transition.js')}}"></script>

<script src="{{url('assets/global/vendor/sortable/Sortable.js')}}"></script>
<script src="{{url('assets/global/vendor/nestable/jquery.nestable.js')}}"></script>


<script src="{{url('assets/global/js/Plugin/sortable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/nestable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/tasklist.js')}}"></script>
<script src="{{url('assets/global/vendor/bootstrap-sweetalert/sweetalert.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/bootstrap-sweetalert.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/responsive-tabs.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/tabs.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/asprogress.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/panel.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/raty.js')}}"></script>
<script src="{{url('assets/global/vendor/select2/select2.full.min.js')}}"></script>
<script src="{{url('assets/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/select2.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/multi-select.js')}}"></script>
<script>
    Config.set('assets', '../../assets');
</script>
<script src="{{asset('assets/examples/js/advanced/bootbox-sweetalert.js')}}"></script>
<script>
    (function(document, window, $) {
        'use strict';

        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>