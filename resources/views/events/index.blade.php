@extends('layouts.dashboard')

@section('content')
<div class="panel">
          <div class="panel-heading">
            <h3 class="panel-title">DEMO CONTENT</h3>
          </div>
          <div class="panel-body">
            <p>Liberatione, pueros dissentio athenis inventore morborum efficiat facere
              conspiratione sensibus diu. Oritur maxime ullius pertinacia commodi
              lectorem primus talem explicatam, erroribus aiebat male torquem num
              sapientiam sollicitudines tollitur, summis mutans pariuntur, paene
              memoria affecta cura aequum, libidinum segnitiae atomi diu molestiae.
              Placebit protervi beatae arbitrarer velim sitisque omnino operosam,
              vicinum sermo utilitatibus, sapientes, tali, cumanum animal peccandi,
              libidinum leniat voluptas ornatum. Temperantiam, desiderat accedunt
              expetenda diis fuerit cupiditate incidant liberamur antipatrum. Angere
              meque deorum. Domo acute parta atilii percipit fortunam. Consuetudinum,
              perturbari geometrica aliena. Animumque dicant dicat magna menandri.
              Depulsa gloriae civibus. Importari civitatis exercitumque dicit sentiamus
              iucundum libro perfecto aliter quod, sedentis contemnere mediocrium
              fatemur linguam molestiam ignota, inportuno quot suscepi disputata
              sententiam. Saluti dominorum percipi legum ipso videri impediri utroque
              mediocrium. Expetenda expediunt, ornatus crudeli infinitis logikh
              delectatum insatiabiles fonte fruitur, manus hominum probarem parum
              copiosae. Iudicem nec acri recordamur detractio angoribus partes,
              persecuti, singulis repellere contineri aequitatem, possent ignem
              vita eidola. Dictum praesidium commodi cupiditate dissentio voce,
              fugiendam tueri panaetium conspiratione hausta cognitione notionem.
              Solam doctiores democriti verbis. Fieri finxerat huic audaces despicationes.
              Umbram diligi synephebos, nemo oporteat horrida memoriter sensu inanitate
              dolorem, dices ingenii commemorandis. Texit sapiens quicquam vidisse
              semper utrumque calere, appellantur exedunt incursione invenerit.</p>
            <p>Vivendo poenis odio repellendus adhibuit saepti, voluptatum. Utramque
              earum iudicari alienus. Elegantis consistat disputationi nostrum
              potiendi ipso sitne alliciat. Perfecto dixissem bene horum amoris
              pararetur gravitate hac corrupisti, aequi eoque malorum torquate
              octavio, laudandis ullius placet illustriora distrahi incurrunt laus
              aptius beatam explicatis. Hoc fungimur mererer. Torqueantur viveremus
              primus omnia p satisfacit evolvendis subtilius aristotele. Desiderent
              quaestionem didicisse suscipit ei fruenda animum exercitationem,
              firmissimum intellegam tollitur conformavit caelo ludus convenire,
              iudicant delectat negent attento, civitas sic inmensae saluto. Numen
              splendide universas dein diogenem sustulisti locum illam, invitat
              torquatos terminari continent peccant explicari delapsa statue susciperet,
              molestia libidinibus emolumenti cupiditatum choro referatur displicet
              maximi diligamus torquentur, sollicitudines nominata vituperari potitur
              quanta isti insipientiam ullus conquisitis geometriaque, concordia
              meminerimus amicos explicari admirer, nominata omnes dictas, ferre
              dicam gratissimo leguntur diligi licet, maximisque, reperire divitiarum,
              exercitationem probarem successerit miserum corrupisti stabilitas,
              solum nemore perspecta sit officiis cumanum pulcherrimum sentiamus,
              silano quasi, perfunctio aristotelem quanto, quanto cui aliquid quaerenda
              quibus benivole, turpius aegritudo magnosque paene explentur opera
              partitio scriptum. Concupiscunt eo quanti, cupiditates vellem ferrentur
              sentiant plusque pacuvii filium etsi, intellegimus cognitio opinor
              adhaesiones, valetudinis multo faciant caecilii nemini antiquitate,
              quidem fugiendum, impediri. Deseruisse vulgo laboramus oderit dolor
              declinare cura laudabilis, offendimur publicam, pugnare.</p>            
          </div>
        </div>
@endsection