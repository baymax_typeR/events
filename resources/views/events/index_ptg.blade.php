@extends('layouts.dashboard')

@section('content')
<div class="page">
    <div class="page-content">
        <!-- Panel -->
        <div class="panel">
            <div class="panel-body">               
                <div class="nav-tabs-horizontal nav-tabs-animate" data-plugin="tabs">                    
                    <div class="tab-content">
                        <div class="tab-pane animation-fade active" id="all_contacts" role="tabpanel">
                            <ul class="list-group">                               
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="pr-0 pr-sm-20 align-self-center">
                                            <div class="avatar avatar-online">
                                                <img src="" alt="#">
                                                <i class="avatar avatar-busy"></i>
                                            </div>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h4 class="mt-0 mb-5">IKCC</h4>
                                            <h5><i class="icon icon-color md-pin" aria-hidden="true"></i>Kaliombo</h5>                                            
                                            <div class="form-group form-material" data-plugin="formMaterial">
                                                <table>
                                                    <thead>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Tanggal</label></td>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Waktu</label></td>
                                                        <td width="60%" colspan="2" align="middle">
                                                            <button type="button"
                                                                class="btn btn-danger btn-sm waves-effect waves-classic">End</button>
                                                        </td>
                                                    </thead>
                                                    <tbody>
                                                        <td>06/07/2020</td>
                                                        <td>13:00 WIB</td>
                                                        <td><button type="button"
                                                                class="btn btn-success btn-sm waves-effect waves-classic">Details</button>
                                                        </td>
                                                    </tbody>
                                                </table>
                                            </div>                                            
                                        </div>
                                    </div>
                                </li>                                
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="pr-0 pr-sm-20 align-self-center">
                                            <div class="avatar avatar-online">
                                                <img src="" alt="#">
                                                <i class="avatar avatar-busy"></i>
                                            </div>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h4 class="mt-0 mb-5">Lotus Hall</h4>
                                            <h5><i class="icon icon-color md-pin" aria-hidden="true"></i>Mojoroto</h5>                                            
                                            <div class="form-group form-material" data-plugin="formMaterial">
                                                <table>
                                                    <thead>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Tanggal</label></td>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Waktu</label></td>
                                                        <td width="60%" colspan="2" align="middle">
                                                            <button type="button"
                                                                class="btn btn-warning btn-sm waves-effect waves-classic">End Show</button>
                                                        </td>
                                                    </thead>
                                                    <tbody>
                                                        <td>06/07/2020</td>
                                                        <td>15:00 WIB</td>
                                                        <td><button type="button"
                                                                class="btn btn-success btn-sm waves-effect waves-classic">Details</button>
                                                        </td>
                                                    </tbody>
                                                </table>
                                            </div>                                            
                                        </div>
                                    </div>
                                </li>                                
                                <li class="list-group-item">
                                    <div class="media">
                                        <div class="pr-0 pr-sm-20 align-self-center">
                                            <div class="avatar avatar-online">
                                                <img src="" alt="#">
                                                <i class="avatar avatar-busy"></i>
                                            </div>
                                        </div>
                                        <div class="media-body align-self-center">
                                            <h4 class="mt-0 mb-5">Grand Surya Hall</h4>
                                            <h5><i class="icon icon-color md-pin" aria-hidden="true"></i>Jl Dhoho</h5>                                            
                                            <div class="form-group form-material" data-plugin="formMaterial">
                                                <table>
                                                    <thead>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Tanggal</label></td>
                                                        <td width="50%" colspan="1"><label class="form-control-label"
                                                                for="inputText">Waktu</label></td>
                                                        <td width="60%" colspan="2" align="middle">
                                                            <button type="button"
                                                                class="btn btn-primary btn-sm waves-effect waves-classic">Ready</button>
                                                        </td>
                                                    </thead>
                                                    <tbody>
                                                        <td>06/07/2020</td>
                                                        <td>17:00 WIB</td>
                                                        <td><button type="button"
                                                                class="btn btn-success btn-sm waves-effect waves-classic">Details</button>
                                                        </td>
                                                    </tbody>
                                                </table>
                                            </div>                                            
                                        </div>
                                    </div>
                                </li>                                
                            </ul>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Panel -->
    </div>
</div>
@endsection