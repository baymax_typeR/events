@extends('layouts.dashboard')

@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-4 col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="pearls pearls-sm row">
                        <div class="pearl current col-2">
                            <div class="pearl-icon"><i class="icon wb-map" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Tempat & Waktu</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon fa-plus-circle" aria-hidden="true"></i></div>
                            <span class="pearl-title">Jumlah Kursi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon fa-hand-o-down" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Kursi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon fa-clone" aria-hidden="true"></i></div>
                            <span class="pearl-title">Konfirmasi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                            <span class="pearl-title">Selesai</span>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-lg-12">
                        {{-- card pemesanan --}}
                        <div class="card pt-2">
                            <div class="card-header mt-5 vertical-align h-40 p-0">
                                <div class=" vertical-align-middle float-left px-15 pt-10 h-40"><i class="icon glyphicon-check" aria-hidden="true"></i></div>
                                <div class=" vertical-align-middle ml-10 font-size-16">Pemesanan Tiket</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">
                                        <div class="example">
                                            <form autocomplete="off">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Tanggal : </label>
                                                    <div class="col-md-9">
                                                        <input type="date" class="form-control" name="name" placeholder="" autocomplete="off" />

                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Jam : </label>
                                                    <div class="col-md-9">
                                                        <input type="time" class="form-control" name="name" placeholder="" autocomplete="off" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Pilih Lokasi </label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" data-plugin="select2" data-placeholder="Select a State" data-allow-clear="true">
                                                            <option></option>
                                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                                <option value="AK">Alaska</option>
                                                                <option value="HI">Hawaii</option>
                                                            </optgroup>
                                                            <optgroup label="Pacific Time Zone">
                                                                <option value="CA">California</option>
                                                                <option value="NV">Nevada</option>
                                                                <option value="OR">Oregon</option>
                                                                <option value="WA">Washington</option>
                                                            </optgroup>
                                                            <optgroup label="Mountain Time Zone">
                                                                <option value="AZ">Arizona</option>
                                                                <option value="CO">Colorado</option>
                                                                <option value="ID">Idaho</option>
                                                                <option value="MT">Montana</option>
                                                                <option value="NE">Nebraska</option>
                                                                <option value="NM">New Mexico</option>
                                                                <option value="ND">North Dakota</option>
                                                                <option value="UT">Utah</option>
                                                                <option value="WY">Wyoming</option>
                                                            </optgroup>
                                                            <optgroup label="Central Time Zone">
                                                                <option value="AL">Alabama</option>
                                                                <option value="AR">Arkansas</option>
                                                                <option value="IL">Illinois</option>
                                                                <option value="IA">Iowa</option>
                                                                <option value="KS">Kansas</option>
                                                                <option value="KY">Kentucky</option>
                                                                <option value="LA">Louisiana</option>
                                                                <option value="MN">Minnesota</option>
                                                                <option value="MS">Mississippi</option>
                                                                <option value="MO">Missouri</option>
                                                                <option value="OK">Oklahoma</option>
                                                                <option value="SD">South Dakota</option>
                                                                <option value="TX">Texas</option>
                                                                <option value="TN">Tennessee</option>
                                                                <option value="WI">Wisconsin</option>
                                                            </optgroup>
                                                            <optgroup label="Eastern Time Zone">
                                                                <option value="CT">Connecticut</option>
                                                                <option value="DE">Delaware</option>
                                                                <option value="FL">Florida</option>
                                                                <option value="GA">Georgia</option>
                                                                <option value="IN">Indiana</option>
                                                                <option value="ME">Maine</option>
                                                                <option value="MD">Maryland</option>
                                                                <option value="MA">Massachusetts</option>
                                                                <option value="MI">Michigan</option>
                                                                <option value="NH">New Hampshire</option>
                                                                <option value="NJ">New Jersey</option>
                                                                <option value="NY">New York</option>
                                                                <option value="NC">North Carolina</option>
                                                                <option value="OH">Ohio</option>
                                                                <option value="PA">Pennsylvania</option>
                                                                <option value="RI">Rhode Island</option>
                                                                <option value="SC">South Carolina</option>
                                                                <option value="VT">Vermont</option>
                                                                <option value="VA">Virginia</option>
                                                                <option value="WV">West Virginia</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="form-group float-right">
                                                    <button type="button" class="btn btn-warning">Cancel</button>
                                                    <a href="{{url('pemesanan2')}}" class="btn btn-primary">Next <i class="icon glyphicon-chevron-right"></i></a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>
@endsection