@extends('layouts.dashboard')

@section('content')
<div class="page-content container-fluid">
    <div class="panel">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-xxl-4 col-lg-12">
                <div class="panel-body">
                    <div class="pearls pearls-sm row">
                        <div class="pearl done col-2">
                            <div class="pearl-icon"><i class="icon wb-map" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Tempat & Waktu</span>
                        </div>
                        <div class="pearl current col-3">
                            <div class="pearl-icon"><i class="icon fa-plus-circle" aria-hidden="true"></i></div>
                            <span class="pearl-title">Jumlah Kursi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon fa-hand-o-down" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Kursi</span>
                        </div>
                        <div class="pearl col-3">
                            <div class="pearl-icon"><i class="icon fa-clone" aria-hidden="true"></i></div>
                            <span class="pearl-title">Konfirmasi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                            <span class="pearl-title">Selesai</span>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-lg-12">
                        {{-- card pemesanan --}}
                        <div class="card pt-2">
                            <div class="card-header mt-5 vertical-align h-40 p-0">
                                <div class=" vertical-align-middle float-left px-15 pt-10 h-40"><i class="icon glyphicon-check" aria-hidden="true"></i></div>
                                <div class=" vertical-align-middle ml-10 font-size-16">Pemesanan Tiket</div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">
                                        <form autocomplete="off">



                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Jumlah Kursi</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" name="name" placeholder="" autocomplete="off" />

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label">Area</label>
                                                <div class="btn-group col-lg-6">
                                                    <div class="">
                                                        <button type="button" class="btn btn-outline w-p100 btn-default dropdown-toggle" id="exampleSizingDropdown2" data-toggle="dropdown" aria-expanded="false">
                                                            Pilih Area&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="exampleSizingDropdown2" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 36px, 0px);">
                                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">VVIP</a>
                                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">VIP</a>
                                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Ekonomi</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group float-right">
                                                <a href="{{url('pemesanan')}}" class="btn btn-warning">Previous</a>
                                                <a href="{{url('pilihKursi')}}" class="btn btn-primary">Next <i class="icon glyphicon-chevron-right"></i></a>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection