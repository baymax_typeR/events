@extends('layouts.dashboard')

@section('content')
<div class="page-content container-fluid">
    <div class="panel">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-xxl-4 col-lg-12">
                <div class="panel-body">
                    <div class="pearls pearls-sm row">
                        <div class="pearl done col-2">
                            <div class="pearl-icon"><i class="icon wb-map" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Tempat & Waktu</span>
                        </div>
                        <div class="pearl done col-3">
                            <div class="pearl-icon"><i class="icon fa-plus-circle" aria-hidden="true"></i></div>
                            <span class="pearl-title">Jumlah Kursi</span>
                        </div>
                        <div class="pearl done col-2">
                            <div class="pearl-icon"><i class="icon fa-hand-o-down" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Kursi</span>
                        </div>
                        <div class="pearl done col-3">
                            <div class="pearl-icon"><i class="icon fa-clone" aria-hidden="true"></i></div>
                            <span class="pearl-title">Konfirmasi</span>
                        </div>
                        <div class="pearl done col-2">
                            <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                            <span class="pearl-title">Selesai</span>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-lg-12">
                        {{-- card pemesanan --}}
                        <div class="card pt-2">
                            <div class="card-header mt-5 vertical-align h-40 p-0">
                                <div class=" vertical-align-middle float-left px-15 pt-10 h-40"><i class="icon glyphicon-check" aria-hidden="true"></i></div>
                                <div class=" vertical-align-middle ml-10 font-size-16">Pemesanan Selesai</div>
                            </div>
                            <div class="card-body mt-10 pt-3 pb-3 font-size-12">
                                <div class="row pt-3">
                                    <div class="col-xxl-4 col-lg-4">

                                        <dl class="dl-horizontal row">
                                            <dt class="col-sm-5">Tanggal</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">17-05-2020</dd>

                                            <dt class="col-sm-5">Jam</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">08.00</dd>

                                            <dt class="col-sm-5">Lokasi</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">Kediri.</dd>

                                            <dt class="col-sm-5">Jumlah Kursi</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">2</dd>

                                            <dt class="col-sm-5">Area</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">A</dd>

                                            <dt class="col-sm-5">Nomor Kursi</dt>
                                            <dd>:</dd>
                                            <dd class="col-sm-5">01,02</dd>
                                        </dl>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-outline btn-default" id="exampleWarningConfirm">OK</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection