@extends('layouts.dashboard')

@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-4 col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="pearls pearls-sm row align-center">
                        <div class="pearl done col-2">
                            <div class="pearl-icon"><i class="icon wb-map" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Tempat & Waktu</span>
                        </div>
                        <div class="pearl done col-3">
                            <div class="pearl-icon"><i class="icon fa-plus-circle" aria-hidden="true"></i></div>
                            <span class="pearl-title">Jumlah Kursi</span>
                        </div>
                        <div class="pearl current col-2">
                            <div class="pearl-icon"><i class="icon fa-hand-o-down" aria-hidden="true"></i></div>
                            <span class="pearl-title">Pilih Kursi</span>
                        </div>
                        <div class="pearl col-3">
                            <div class="pearl-icon"><i class="icon fa-clone" aria-hidden="true"></i></div>
                            <span class="pearl-title">Konfirmasi</span>
                        </div>
                        <div class="pearl col-2">
                            <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                            <span class="pearl-title">Selesai</span>
                        </div>
                    </div>
                    <div class="col-xxl-12 col-lg-12">
                        {{-- card pemesanan --}}
                        <div class="card pt-2">
                            <div class="card-header mt-5 vertical-align h-40 p-0">
                                <div class=" vertical-align-middle float-left px-15 pt-10 h-40"><i class="icon glyphicon-check" aria-hidden="true"></i></div>
                                <div class=" vertical-align-middle ml-10 font-size-16">Pemesanan Tiket</div>
                            </div>
                            <div class="card-body">
                                <div class="panel shadow panel-borderedd">
                                    <div class="panel-heading vertical-align">
                                        <h3 class="panel-title text-center">Depan</h3>
                                    </div>
                                    <div class="panel-body pb-15 pt-15 bg-blue-grey-200">
                                        <div class="row ">
                                            <div class="col-xxl-12 col-lg-12">
                                                <!-- Example Tabs In The Panel -->
                                                <div class="example-wrap">
                                                    <div class="nav-tabs-vertical nav-tabs-inverse" data-plugin="tabs">
                                                        <ul class="nav nav-tabs nav-tabs-solid" role="tablist">
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseOne" aria-controls="exampleTabsSolidLeftInverseOne" role="tab">
                                                                    Area 1
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseTwo" aria-controls="exampleTabsSolidLeftInverseTwo" role="tab">
                                                                    Area 2
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 3
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 4
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 5
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 6
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 7
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 8
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 9
                                                                </a>
                                                            </li>
                                                            <li class="nav-item" role="presentation">
                                                                <a class="nav-link" data-toggle="tab" href="#exampleTabsSolidLeftInverseThree" aria-controls="exampleTabsSolidLeftInverseThree" role="tab">
                                                                    Area 10
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content p-0">
                                                            <div class="tab-pane active" id="exampleTabsSolidLeftInverseOne" role="tabpanel">
                                                                <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' A' }}
                                                                            @else
                                                                            {{ $def++.' A' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' B' }}
                                                                            @else
                                                                            {{ $def++.' B' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' C' }}
                                                                            @else
                                                                            {{ $def++.' C' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' D' }}
                                                                            @else
                                                                            {{ $def++.' D' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' E' }}
                                                                            @else
                                                                            {{ $def++.' E' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' F' }}
                                                                            @else
                                                                            {{ $def++.' F' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) @if($def==5) <button type="button" class="btn btn-icon btn-sm btn-danger m-2 w-lg-60">
                                                                            @else
                                                                            <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                                @endif
                                                                                @if(Str::length($def)==1)
                                                                                {{ $def++.' G' }}
                                                                                @else
                                                                                {{ $def++.' G' }}
                                                                                @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' H' }}
                                                                            @else
                                                                            {{ $def++.' H' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' I' }}
                                                                            @else
                                                                            {{ $def++.' I' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-2 col-lg-1">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-60">
                                                                            @if(Str::length($def)==1)
                                                                            {{ $def++.' J' }}
                                                                            @else
                                                                            {{ $def++.' J' }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="tab-pane" id="exampleTabsSolidLeftInverseTwo" role="tabpanel">
                                                                <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                                    <div class="col-xxl-4 col-lg-4">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-default btn-outline m-2 w-md-75">
                                                                            <i class="icon fa-diamond" aria-hidden="true"></i> &nbsp;
                                                                            @if(Str::length($def)==1)
                                                                            {{ '0'.$def++ }}
                                                                            @else
                                                                            {{ $def++ }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-4 col-lg-4">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-default btn-outline m-2 w-md-75">
                                                                            <i class="icon fa-diamond" aria-hidden="true"></i> &nbsp;
                                                                            @if(Str::length($def)==1)
                                                                            {{ '0'.$def++ }}
                                                                            @else
                                                                            {{ $def++ }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-4 col-lg-4">

                                                                        @for ($i = $def; $i <= $vip; $i++) <button type="button" class="btn btn-icon btn-sm btn-default m-2 btn-outline w-md-75">
                                                                            <i class="icon fa-diamond" aria-hidden="true"></i> &nbsp;
                                                                            @if(Str::length($def)==1)
                                                                            {{ '0'.$def++ }}
                                                                            @else
                                                                            {{ $def++ }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="exampleTabsSolidLeftInverseThree" role="tabpanel">
                                                                <div class="row" data-plugin="matchHeight" data-by-row="true">
                                                                    <div class="col-xxl-6 col-lg-6">

                                                                        @for ($i = $def; $i <= $eco; $i++) <button type="button" class="btn btn-icon btn-lg btn-default btn-outline m-5 w-md-80 font-size-14">
                                                                            <i class="icon fa-group" aria-hidden="true"></i> &nbsp;
                                                                            @if(Str::length($def)==1)
                                                                            {{ '0'.$def++ }}
                                                                            @else
                                                                            {{ $def++ }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor
                                                                            <div style="display:none">{{$def=1}}</div>

                                                                    </div>
                                                                    <div class="col-xxl-6 col-lg-6">

                                                                        @for ($i = $def; $i <= $eco; $i++) <button type="button" class="btn btn-icon btn-lg btn-default btn-outline m-5 w-md-80 font-size-14">
                                                                            <i class="icon fa-group" aria-hidden="true"></i> &nbsp;
                                                                            @if(Str::length($def)==1)
                                                                            {{ '0'.$def++ }}
                                                                            @else
                                                                            {{ $def++ }}
                                                                            @endif
                                                                            </button>
                                                                            @endfor

                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- End Example Tabs In The Panel -->

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <button type="button" class="btn btn-icon btn-sm btn-danger m-2 w-lg-30 h-30">
                                    </button>
                                    <h5>Sudah Di Booking</h5>
                                    <button type="button" class="btn btn-icon btn-sm btn-success m-2 w-lg-30 h-30">
                                    </button>
                                    <h5>Belum Di Booking</h5>
                                    <button type="button" class="btn btn-icon btn-sm btn-primary m-2 w-lg-30 h-30">
                                    </button>
                                    <h5>Pilihan</h5>
                                </div>
                                <div class="form-group float-right">
                                    <a href="{{url('pemesanan2')}}" class="btn btn-warning">Previous</a>
                                    <a href="{{url('konfirmasi')}}" class="btn btn-primary">Next <i class="icon glyphicon-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>

</div>
@endsection