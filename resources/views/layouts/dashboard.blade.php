<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>TIKET KU</title>
    @include('configs.assets')
</head>

<body class="animsition dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    {{-- Navbar  --}}
    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega " role="navigation">

        <div class="navbar-header bg-orange-700 ">

            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
                <i class="icon wb-more-horizontal" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-left site-gridmenu-toggle " data-toggle="gridmenu">
                <img class="navbar-brand-logo" src="{{ asset('assets/images/saya.webp')}}" title="Simpeg">
                <span class="navbar-brand-text hidden-xs-down"> TIKET </span>
            </div>
        </div>

        <div class="navbar-container container-fluid blue-grey-700">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-float" id="toggleMenubar">
                        <a class="nav-link" data-toggle="menubar" href="#" role="button">
                            <i class="icon hamburger hamburger-arrow-left">
                                <span class="sr-only">Toggle menubar</span>
                                <span class="hamburger-bar"></span>
                            </i>
                        </a>
                    </li>
                    <li class="nav-item vertical-align" id="toggleMenubar">
                        <div class="vertical-align-middle py-0 mt-5">
                            <h4 class="grey-700">Tiket Ku</h4>
                        </div>
                    </li>
                </ul>

                <!-- End Navbar Toolbar -->
                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
                            <span class="avatar avatar-online">
                                <img src="{{ asset('assets/images/profile.jpg')}}" alt="...">
                                <i></i>
                            </span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Profile</a>
                            <div class="dropdown-divider" role="presentation"></div>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Logout</a>
                        </div>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->

        </div>
    </nav>



    {{-- SideBar  --}}
    <div class="site-menubar">
        <div class="site-menubar-body">
            <div>
                <div>
                    <ul class="site-menu" data-plugin="menu">
                        <li class="site-menu-category">MAIN MENU</li>
                        <li class="site-menu-item has-sub">
                            <a href="{{url('/dasboard')}}">
                                <i class="site-menu-icon wb-home" aria-hidden="true"></i>
                                <span class="site-menu-title">PETUGAS</span>
                            </a>
                        </li>

                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <i class="site-menu-icon wb-envelope" aria-hidden="true"></i>
                                <span class="site-menu-title">PENGUNJUNG</span>
                            </a>
                        </li>
                        {{-- akhir pesan masuk --}}
                    </ul>
                </div>
            </div>
        </div>

        <div class="site-menubar-footer">
            <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                <span class="icon wb-power" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                <span class="icon wb-eye-close" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Setting">
                <span class="icon wb-wrench" aria-hidden="true"></span>
            </a>
        </div>
    </div>

    {{-- grid menu   saat tombol span dibuka--}}
    <div class="site-gridmenu">
        <div>
            <div>
                <ul>
                    <li>
                        <a href="#">
                            <i class="icon wb-home"></i>
                            <span>PETUGAS</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon wb-folder"></i>
                            <span>PENGUNJUNG</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>


    {{-- Content --}}

    <div class="page bg-blue-grey-100">
        @yield('content')

    </div>
    {{-- Footer --}}
    <footer class="site-footer bg-orange-700 blue-grey-100">
        <div class="site-footer-legal">© TIKET KU </div>
        <div class="site-footer-right" style='color:white;'>2018 | CAH CAH </div>
    </footer>

</body>
@include('configs.script')

</html>