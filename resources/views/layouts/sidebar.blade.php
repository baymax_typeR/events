<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SIMPEG KEDIRI KOTA</title>

    @include('configs.assets')
</head>

<body class="animsition dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    {{-- Navbar  --}}
    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega " role="navigation">

        <div class="navbar-header bg-purple-800 ">

            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
                <i class="icon wb-more-horizontal" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-center site-gridmenu-toggle " data-toggle="gridmenu">
                <img class="navbar-brand-logo" src="{{ asset('assets/images/logo_pemkot.png')}}" title="Simpeg">
                <span class="navbar-brand-text hidden-xs-down"> SIMPEG </span>
            </div>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search" data-toggle="collapse">
                <span class="sr-only">Toggle Search</span>
                <i class="icon wb-search" aria-hidden="true"></i>
            </button>
        </div>

        <div class="navbar-container container-fluid bg-purple-500 blue-grey-100">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-float" id="toggleMenubar">
                        <a class="nav-link" data-toggle="menubar" href="#" role="button">
                            <i class="icon hamburger hamburger-arrow-left">
                                <span class="sr-only">Toggle menubar</span>
                                <span class="hamburger-bar"></span>
                            </i>
                        </a>
                    </li>
                    <li class="nav-item vertical-align" id="toggleMenubar">
                        <div class="vertical-align-middle">

                            @if (request()->is('*diklat_teknis'))
                            <h4 class="grey-100">Riwayat Diklat Teknis</h4>
                            @endif
                            @if (request()->is('*seminarLokakarya'))
                            <h4 class="grey-100">Riwayat Seminar Lokakarya</h4>
                            @endif
                            @if (request()->is('*penataran'))
                            <h4 class="grey-100">Riwayat Penataran</h4>
                            @endif
                            @if (request()->is('*penghargaan'))
                            <h4 class="grey-100">Riwayat Penghargaan</h4>
                            @endif
                            @if (request()->is('*kinerja'))
                            <h4 class="grey-100">Riwayat Penilaian Kinerja</h4>
                            @endif
                            @if (request()->is('*organisasi'))
                            <h4 class="grey-100">Riwayat Organisasi</h4>
                            @endif
                            @if (request()->is('*dasboard'))
                            <h4 class="grey-100">Dasboard</h4>
                            @endif
                            @if (request()->is('*riwayat_gaji_berkala'))
                            <h4 class="grey-100">Riwayat Gaji Berkala</h4>
                            @endif
                            @if (request()->is('*riwayat_jabatan'))
                            <h4 class="grey-100">Riwayat Jabatan</h4>
                            @endif
                            @if (request()->is('*angka_kredit'))
                            <h4 class="grey-100">Riwayat Angka Kredit</h4>
                            @endif
                            @if (request()->is('*data_orang_tua'))
                            <h4 class="grey-100">Data Orang Tua</h4>
                            @endif

                            @if (request()->is('*istri_suami'))
                            <h4 class="grey-100">Data Istri/Suami</h4>
                            @endif
                            @if (request()->is('*anak'))
                            <h4 class="grey-100">Data Anak</h4>
                            @endif
                            @if (request()->is('*saudara'))
                            <h4 class="grey-100">Data Saudara</h4>
                            @endif
                            @if (request()->is('*pendidikan'))
                            <h4 class="grey-100">Riwayat Pendidikan</h4>
                            @endif
                            @if (request()->is('*tempat_kerja'))
                            <h4 class="grey-100">Riwayat Tempat Kerja</h4>
                            @endif
                            @if (request()->is('*dkl_kep'))
                            <h4 class="grey-100">Riwayat Pendidikan & Pelatihan Kepemimpinan/Struktural</h4>
                            @endif
                            @if (request()->is('*dkl_fung'))
                            <h4 class="grey-100">Riwayat Diklat Fungsional</h4>
                            @endif

                        </div>
                    </li>                    
                </ul>
                
                <!-- End Navbar Toolbar -->
                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
                            <span class="avatar avatar-online">
                                <img src="{{ asset('assets/images/profile.jpg')}}" alt="...">
                                <i></i>
                            </span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Profile</a>
                            <div class="dropdown-divider" role="presentation"></div>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Logout</a>
                        </div>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->

            <!-- Site Navbar Seach -->
            <div class="collapse navbar-search-overlap" id="site-navbar-search">
                <form role="search">
                    <div class="form-group">
                        <div class="input-search">
                            <i class="input-search-icon wb-search" aria-hidden="true"></i>
                            <input type="text" class="form-control" name="site-search" placeholder="Search...">
                            <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search" data-toggle="collapse" aria-label="Close"></button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End Site Navbar Seach -->
        </div>
    </nav>



    {{-- SideBar  --}}
    <div class="site-menubar">
        <div class="site-menubar-body">
            <div>
                <div>
                    <ul class="site-menu" data-plugin="menu">
                        <li class="site-menu-category">MAIN MENU</li>
                        <li class="site-menu-item has-sub">
                            <a href="{{url('/dasboard')}}">
                                <i class="site-menu-icon wb-home" aria-hidden="true"></i>
                                <span class="site-menu-title">Home</span>
                            </a>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon wb-folder" aria-hidden="true"></i>
                                <span class="site-menu-title">Master Data</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Struktur Organisasi</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Eselon</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Pangkat(Gol/Ruang)</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Status Pegawai</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Pendidikan Formal</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Daerah</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Gaji Pokok</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Jenis Pegawai</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Diklat Kepemimpinan</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Agama</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Jabaatan Fungsional</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Jenis Kenaikan Pangkat</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Kedudukan Pegawai</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="#">
                                        <span class="site-menu-title wb-chevron-right-mini">&emsp;Bidang Studi</span>
                                    </a>
                                </li>
                        </li>
                        <li class="site-menu-item has-sub">
                            <a class="animsition-link" href="#">
                                <span class="site-menu-title wb-chevron-right-mini">&emsp;Pejabat Menetapkan</span>
                            </a>

                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="layouts/aside-left-static.html">
                                        <span class="site-menu-title">Left Static</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="layouts/aside-right-static.html">
                                        <span class="site-menu-title">Right Static</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="layouts/aside-left-fixed.html">
                                        <span class="site-menu-title">Left Fixed</span>
                                    </a>
                                </li>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="layouts/aside-right-fixed.html">
                                        <span class="site-menu-title">Right Fixed</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                    {{-- akhir master data --}}


                    {{-- Jabatan --}}
                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-star" aria-hidden="true"></i>
                            <span class="site-menu-title">Jabatan</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Struktural</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Fungsional Umum</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Fungsional Khusus</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Rekapitulasi Jabatan</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- Akhir jabatan --}}
                    <li class="site-menu-item has-sub">
                        <a class="animsition-link" href="#">
                            <i class="site-menu-icon wb-minus-circle" aria-hidden="true"></i>
                            <span class="site-menu-title">Belum Verifikasi</span>
                        </a>
                    </li>

                    {{-- Akhir Belum Verifikasi --}}

                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-user" aria-hidden="true"></i>
                            <span class="site-menu-title">Pegawai</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="{{ url('/pegawai') }}">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Daftar Pegawai</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Tambah Pegawai</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Pindah Pegawai</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Report Pegawai</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-bookmark" aria-hidden="true"></i>
                            <span class="site-menu-title">Mutasi</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Antar Satker</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Fungsional Tertentu</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Riwayat</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{--Akhir Mutasi --}}

                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-graph-up" aria-hidden="true"></i>
                            <span class="site-menu-title">Kenaikan Pangkat</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Daftar Pengajuan</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Prediksi</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Riwayat</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- Akhir Kenaikan pangkat --}}


                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-list" aria-hidden="true"></i>
                            <span class="site-menu-title">Gaji Berkala</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Daftar Pengajuan</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Prediksi</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Riwayat</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- akhir gaji berkala --}}



                    <li class="site-menu-item has-sub">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon wb-briefcase" aria-hidden="true"></i>
                            <span class="site-menu-title">Pensiun</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Pensiun Dini</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Pensiun Batas Usia</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Pensiun Janda Duda</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link" href="#">
                                    <span class="site-menu-title wb-chevron-right-mini">&emsp;Riwayat Pensiun</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- akhir PEnsiun --}}



                    <li class="site-menu-item has-sub">
                        <a class="animsition-link" href="#">
                            <i class="site-menu-icon wb-envelope" aria-hidden="true"></i>
                            <span class="site-menu-title">Pesan Masuk</span>
                        </a>
                    </li>
                    {{-- akhir pesan masuk --}}


                    <li class="site-menu-item has-sub">
                        <a class="animsition-link" href="#">
                            <i class="site-menu-icon wb-folder" aria-hidden="true"></i>
                            <span class="site-menu-title">Disiplin</span>
                        </a>
                    </li>
                    {{-- akhir Disiplin --}}

                    <li class="site-menu-item has-sub">
                        <a class="animsition-link" href="#">
                            <i class="site-menu-icon wb-folder" aria-hidden="true"></i>
                            <span class="site-menu-title">Diklat Antar Jabatan</span>
                        </a>
                    </li>
                    {{-- Akhir Diklat Jabatan --}}

                    <li class="site-menu-item has-sub">
                        <a class="animsition-link" href="#">
                            <i class="site-menu-icon wb-wrench" aria-hidden="true"></i>
                            <span class="site-menu-title">Ganti Password</span>
                        </a>
                    </li>
                    {{-- Akhir Ganti pass --}}

                    </ul>
                </div>
            </div>
        </div>

        <div class="site-menubar-footer">
            <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                <span class="icon wb-power" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                <span class="icon wb-eye-close" aria-hidden="true"></span>
            </a>
            <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Setting">
                <span class="icon wb-wrench" aria-hidden="true"></span>
            </a>
        </div>
    </div>

    {{-- grid menu   saat tombol span dibuka--}}
    {{-- <div class="site-gridmenu">
          <div>
            <div>
              <ul>
                <li>
                  <a href="#">
                    <i class="icon wb-home"></i>
                    <span>Home</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-folder"></i>
                    <span>Master Data</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-star"></i>
                    <span>Jabatan</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-minus-circle"></i>
                    <span>Belum Verifikasi</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-user"></i>
                    <span>Pegawai</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-file"></i>
                    <span>Mutasi</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i class="icon wb-signal"></i>
                    <span>Kenaikan Pangkat</span>
                  </a>
                </li>
                <li>
                    <a href="#">
                      <i class="icon wb-list-numbered"></i>
                      <span>Gaji Berkala</span>
                    </a>
                  </li>
                <li>
                  <a href="#">
                    <i class="icon wb-list-numbered"></i>
                    <span>Gaji Berkala</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div> --}}


    {{-- Content --}}

    <div class="page bg-blue-grey-700">
        @yield('content')
    </div>

    {{-- Footer --}}
    <footer class="site-footer bg-purple-500 blue-grey-100">
        <div class="site-footer-legal">© BKD Kota Kediri </div>
        <div class="site-footer-right" style='color:white;'>2018 | Simpeg Kediri Kota </div>
    </footer>

</body>
@include('configs.script')

</html>