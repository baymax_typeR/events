@extends('layouts.dashboard')

@section('content')
<div class="page-content container-fluid">
<div class="row" data-plugin="matchHeight" data-by-row="true">
<div class="col-xxl-4 col-lg-6">
    <!-- Example Tabs In The Panel -->
    <div class="panel nav-tabs-horizontal" data-plugin="tabs">
      <div class="panel-heading">
        <h3 class="panel-title">Petugas</h3>
      </div>
      <ul class="nav nav-tabs nav-tabs-line" role="tablist">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome"
            aria-controls="exampleTopHome" role="tab" aria-expanded="true"><i class="icon wb-user" aria-hidden="true"></i>List Penumpang</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents"
            aria-controls="exampleTopComponents" role="tab"><i class="icon wb-user" aria-hidden="true"></i>Components</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss"
            role="tab"><i class="icon wb-tag" aria-hidden="true"></i>CSS</a></li>
      </ul>
      <div class="panel-body">
        <div class="tab-content">
          <div class="tab-pane active" id="exampleTopHome" role="tabpanel">
            Conturbamur senectutem saepti statua, putanda homini acuti dialectica levamur urbanitas
            animadversionem mala se depulsa, magis nihilo efficeret tenetur
            cives gloriae concursio defendit conscientiam nascuntur, refugiendi
            defendit falsi platonem paranda metum iustioribus cognita robustus,
            tuum omnia usque omnis temperantiam quaeri electram.
          </div>
          <div class="tab-pane" id="exampleTopComponents" role="tabpanel">
            Protervi dissensio consuetudine equos publicam ingenia. Voluptatibus legendus initia
            confirmare sententiam. Desistunt possint habeatur dediti dubio,
            triarium is offendimur reprehenderit exercitus laudabilis motus
            celeritas, utrum dissentio renovata, habet partus natus. Iustius
            disserunt, quantum ennii admodum divinum mortem elaborare primum
            autem.
          </div>
          <div class="tab-pane" id="exampleTopCss" role="tabpanel">
            Incurrunt latinam, faciendi dedecora evertitur delicatissimi, afficit noctesque
            detracta illustriora epicurum contenta rogatiuncula dolores
            perspecta indocti, eveniunt confirmatur tractat consuevit durissimis
            iuvaret coercendi familiarem. Dolere prima fortunae intellegamus
            vix porro huic errorem molestum, graecos deinde effugiendorum
            aliter appetendum afferrent eosdem.
          </div>
        </div>
      </div>
    </div>
    <!-- End Example Tabs In The Panel -->
  </div>
</div>
</div>
@endsection
