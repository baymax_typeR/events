<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('layouts/dashboard');
});

Route::get('/pemesanan', function () {
    return view('events/pengunjung/pemesanan');
});

Route::get('/pemesanan2', function () {
    return view('events/pengunjung/pemesanan2');
});

Route::get('/konfirmasi', function () {
    return view('events/pengunjung/konfirmasi_pemesanan');
});

Route::get('/pemesanan_selesai', function () {
    return view('events/pengunjung/pemesanan_selesai');
});

Route::get('/index', function () {
    return view('events/index');
});

Route::get('/pilihKursi', 'dataPengunjung@pilihKursi');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/index_ptg', function () {
    return view('events/index_ptg');
});
